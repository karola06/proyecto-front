import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js'
import '../login-usuario/login-usuario.js'
import '../alta-usuario/alta-usuario.js'
import '../app-route-cuentas/app-route-cuentas.js'

/**
 * @customElement
 * @polymer
 */
class AppRoute extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .jumbotron{
          !color: #D68910;
          color: #A85404;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1 class="jumbotron" style="background-color: #D3A981">Banco TechU</h1>
      <iron-pages selected="[[componentName]]" attr-for-selected="component-name" default-selected="login">
        <div component-name="login"><login-usuario class="text-center"
            on-evento-login="processEvent"
            on-evento-altausuario="processEventAltaUsuario">
        </login-usuario></div>
        <div component-name="altausuario"><alta-usuario alta="[[alta]]"
            on-evento-volverloginusu="processEventVolverLoginUsu">
        </alta-usuario></div>
        <div component-name="usuario"><app-route-cuentas idusuario="[[idusuario]]"
            on-evento-logout="processEventLogout">
        </app-route-cuentas></div>
      </iron-pages>
    `;
  }

  constructor() {
        console.log("CONSTRUCTOR APP-ROUTE");
        super();
        if (this.componentName == null) {
          this.componentName="login";
        }
  }

  static get properties() {
    console.log("get properties app-route-test");
    console.log("componentName " + this.componentName);
    return {
        componentName:{
        type:  String
      }, idcuenta:{
        type:  String
      }
    };
  }  //  End properties

  processEvent(e){
    console.log("Capturado evento login del emisor--------------------");
    console.log(e);
    this.componentName="usuario";
    this.idusuario=e.detail.idusuario;
  }

  processEventAltaUsuario(e){
    console.log("Capturado evento altausuario del emisor-----------------");
    console.log(e);
    this.componentName="altausuario";
  }

  processEventVolverLoginUsu(e){
    console.log("Capturado evento volverlogin de app-route-cuenta----------------");
    console.log(e);
    console.log("error " + e.detail.error);
    if (e.detail.error==true){
      this.componentName="altausuario";
    } else {
      this.componentName="login";
    }
  }

  processEventLogout(e){
    console.log("Capturado evento logout del emisor----------------");
    console.log(e);
    this.idusuario="";
    if (e.detail.error){
      this.componentName="usuario";
    } else {
      this.componentName="login";
    }
  }

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('app-route-inicio', AppRoute);
