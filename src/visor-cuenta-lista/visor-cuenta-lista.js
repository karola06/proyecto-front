import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
//import {html, PolymerElement} from '@polymer/lib/elements/dom-repeat';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuentaLista extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*el estilo no entra al componente ----
              all: initial; */
          border-style: solid;
          border-width: medium;
          border-color: #A85404;
        }

        .redbg {
          background-color: red;
        }
        .bluebg {
          background-color: blue;
        }
        .greenbg {
          background-color: green;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <div> <h3 style="margin-left: 10px">Lista de cuentas:</h3> </div>
      <div class="table-responsive-sm">
      <table class="table table-sm table-striped" name="kk" id="tablacuentas">
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Cuenta</th>
            <th scope="col">Saldo</th>
          </tr>
        </thead>
        <tbody id="datoscuentas">
          <template id="listacuentas" is="dom-repeat" items="{{cuentas}}">
            <tr id="lineacuentas-[[index]]">
              <td><input type="radio" id="selecion" name="cuentaseleccionada" on-click="toggleSelection" value={{item.IBAN}}/></td>
              <td>{{item.IBAN}}</td>
              <td align="right">{{item.balance}}</td>
            </tr>
            </template>
        </tbody>
      </table>
      </div>

      <button class="btn btn-warning" on-click="altacta">Alta Cuenta</button>
      <button class="btn btn-warning" on-click="consultamov">Movimientos</button>
      <button class="btn btn-warning" on-click="altamov">Alta Movimientos </button>
      <button class="btn btn-warning" on-click="altamovdiv">Alta Movimientos Divisa</button>
      <button class="btn btn-warning" on-click="logout">Logout</button>
      <div></div>
      <span hidden$="[[!error]]">[[msgerror]]</span>

      <iron-ajax
        id="getCuentas"
        url="http://localhost:3000/apitechu/cuentas/{{idusuario}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
      <iron-ajax
        id="doAltacta"
        url="http://localhost:3000/apitechu/cuenta/{{idusuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error ="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="doLogout"
        url="http://localhost:3000/apitechu/logout/{{idusuario}}"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponselogout"
        on-error ="showErrorlogout"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      idusuario: {
        type: Number,
        observer: "_idusuarioChanged"
      }, cuentas: {
        type : Array
      }, refresco: {
        type: Boolean,
        observer: "_refrescoChanged"
      }, cuentaseleccionada: {
        type : String
      }, balanceseleccionado: {
        type : String
      }

    };
  }  // End properties

  altacta(){
      console.log("Botón alta cuentas");
      this.error = false;
      this.msgerror = "";
      var ctaData = {
      }
      //  this.$ es para acceder a los elementos del html
      this.$.doAltacta.body = JSON.stringify(ctaData);
      // comunica con el back-end lanzando la petición
      this.$.doAltacta.generateRequest();
}

  toggleSelection(e) {
    console.log("toggle posicion " + e);
//    e.preventDefault();
    var el = e.target;
    var item = this.$.listacuentas.itemForElement(el);
    console.log(item);
    this.cuentaseleccionada = item.IBAN;
    this.balanceseleccionado = item.balance;
    console.log("cuentaseleccionada " + this.cuentaseleccionada);
    console.log("balanceseleccionado " + this.balanceseleccionado);
  }

  altamov(){
    console.log("Botón altamovimientos");
    if (this.cuentaseleccionada=="" || this.cuentaseleccionada == null){
      this.error = true;
      this.msgerror = "Es necesario seleccionar una cuenta";
    } else {
    this.error = false;
    this.msgerror = "";
    this.dispatchEvent(
      new CustomEvent(
        "evento-altamovimientos",
        {
          "detail" : {
            "cuentaseleccionada" : this.cuentaseleccionada
          }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
        }
      )  //end parámetros new CustomEvent
    )
    }  //end parámetros dispatchEvent
  }

  altamovdiv(){
    console.log("Botón altamovimientos divisa");
    if (this.cuentaseleccionada=="" || this.cuentaseleccionada == null){
      this.error = true;
      this.msgerror = "Es necesario seleccionar una cuenta";
    } else {
    this.error = false;
    this.msgerror = "";
    this.dispatchEvent(
      new CustomEvent(
        "evento-altamovimientosdiv",
        {
          "detail" : {
            "cuentaseleccionada" : this.cuentaseleccionada
          }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
        }
      )  //end parámetros new CustomEvent
    )  //end parámetros dispatchEvent
    }
  }

  manageAJAXresponse(data){
    console.log("Llegaron los resultados ");
    console.log(data.detail.response);
  //  si el login es correcto lanzo el evento
    console.log("Botón pulsado ===========================");
    console.log("*****Alta cuenta/consulta****");
    this.$.getCuentas.generateRequest();
  }   //End manageAJAXresponse

  manageAJAXresponselogout(data){
    console.log("Llegaron los resultados del logout ");
    console.log(data.detail.response);
  }

  showErrorlogout(error){
    console.log("Hubo un error ");
    console.log(error);
  }  // End showError

  showError(error){
    console.log("Hubo un error ");
    console.log(error);
  }  // End showError

  logout(){
    console.log("Botón Logout ======");
    this.error = false;
    this.msgerror = "";
    var ctaData = {
    }
    this.cuentas = [];
    this.$.doLogout.body = JSON.stringify(ctaData);
    this.$.doLogout.generateRequest();
    this.cuentaseleccionada="";
      console.log("Botón logout visor-cuenta-lista");
      this.dispatchEvent(
        new CustomEvent(
          "evento-logout",
          {
            "detail" : {
            }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
          }
        )  //end parámetros new CustomEvent
      )  //end parámetros dispatchEvent
  }

  consultamov(){
    console.log("Botón consulta movimientos");
    if (this.cuentaseleccionada=="" || this.cuentaseleccionada == null){
      this.error = true;
      this.msgerror = "Es necesario seleccionar una cuenta";
    } else {
      if (this.balanceseleccionado=="0.00"){
      this.error = true;
      this.msgerror = "No hay movimientos";
      } else {
        this.error = false;
        this.msgerror = "";
        this.refresco=false;   /******/
        this.dispatchEvent(
          new CustomEvent(
            "evento-consultamovimientos",
            {
              "detail" : {
                "cuentaseleccionada" : this.cuentaseleccionada,
                "refrescomov" : true,
                "refresco" : false
              }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
            }
          )  //end parámetros new CustomEvent
        )  //end parámetros dispatchEvent
      }
    }
  }

_refrescoChanged(newValue, oldValue) {
  console.log("*****Refrescar****==========");
  console.log("refresco value has changed");
  console.log("Old value was " + oldValue);
  console.log("New value is " + newValue);
  if (this.refresco) {
   this.$.getCuentas.generateRequest();
  }
 }

  _idusuarioChanged() {
    console.log("idusuario app-route-cuentas " + this.idusuario);
    if (this.idusuario != "" && this.idusuario!= null){
      console.log("*****Cambia usuario****");
      this.$.getCuentas.generateRequest();
    }
  }

// función showData manejadora de la respuesta
  showData(data){
    if (data.detail.response.status =="NOK"){
      this.error = true;
      this.msgerror = data.detail.response.msg;
      this.cuentas = [];
    } else {
    console.log("showData visor-cuenta-lista");
    console.log(data.detail.response);
    this.cuentas=data.detail.response;
    let cuentaaux = this.cuentas;
    for (var i=0; i < cuentaaux.length; i++){
      if (cuentaaux[i].IBAN==this.cuentaseleccionada){
        this.balanceseleccionado = cuentaaux[i].balance;
        console.log("balance con for " + this.balanceseleccionado);
        break;
      }
    }
    console.log("cuentas ========= " + this.cuentas);
    }
  }

}  //  End Class

window.customElements.define('visor-cuenta-lista', VisorCuentaLista);
