import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorMovLista extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*el estilo no entra al componente ----
              all: initial; */
              border-style: solid;
              border-width: medium;
              border-color: #A85404;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <div>
      <h3>Listado de movimientos:</h3>

      <h6>{{idcuenta}}</h6>
      </div>
      <div class="table-responsive-sm">
      <table class="table table-sm table-striped">
        <thead>
          <tr>
            <th scope="col">Fecha</th>
            <th scope="col">Concepto</th>
            <th scope="col">Tipo</th>
            <th scope="col">Importe</th>
          </tr>
        </thead>
        <tbody>
          <template is="dom-repeat" items="{{movimientos}}">
            <tr>
              <td>{{item.fechamov}}</td>
              <td>{{item.concepto}}</td>
              <td align="center">{{item.tipomov}}</td>
              <td align="right">{{item.importe}}</td>
            </tr>
          </template>
        </tbody>
      </table>
      </div>
      <button class="btn btn-warning" on-click="volvercta">Volver</button>
      <span hidden$="[[!error]]">[[msgerror]]</span>

      <iron-ajax
        id="getMovimientos"
        url="http://localhost:3000/apitechu/movimientos/{{idcuenta}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    console.log("get properties movimientos");
    return {
      idcuenta: {
        type: String
      }, movimientos: {
        type : Array
      }, refrescomov: {
        type : Boolean,
        observer: "_idrefrescomov"
      }
    };
  }  // End properties

   _idrefrescomov(newValue, oldValue) {
     console.log("Lista movimientos-refrescomov changed");
     if (this.refrescomov) {
       if (this.idcuenta != '' && this.idcuenta !=null){
         this.$.getMovimientos.generateRequest();
       }
     }
     this.refrescomov=false;
      console.log("Old value was " + oldValue);
      console.log("New value is " + newValue);
    }

  showData(data){
    if (data.detail.response.status =="NOK"){
      this.error = true;
      this.msgerror = data.detail.response.msg;
      this.movimientos = [];
    } else {
      this.error = false;
      this.msgerror = "";
      console.log("showData movimientos " + this.idcuenta);
      console.log(data.detail.response);
      this.movimientos=data.detail.response;
      this.refrescomov=false;
    }
  }

  volvercta(){
      console.log("Botón volver visor-mov-lista");
      this.movimientos=[];
      this.refrescomov=false;
      this.dispatchEvent(
        new CustomEvent(
          "evento-volver-list-cta",
          {
            "detail" : {
              "refresco" : true,
              "refrescomov" :false
            }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
          }
        )  //end parámetros new CustomEvent
      )  //end parámetros dispatchEvent
  }

}  //  End Class

// define una etiqueta y la asocia con la clase (FrontproyectoApp)
window.customElements.define('visor-mov-lista', VisorMovLista);
