import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaMovimiento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        .lh-condensed { line-height: 1.25; }

        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <div class="col-md-8 order-md-1">
      <h4 class="mb-3">Datos movimiento cuenta:</h4>
      <h5 class="mb-3">{{idcuentalta::input}}</h5>
        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="fechamov">Fecha</label>
            <input type="date" class="form-control" id="fechamov" placeholder="dd/mm/yyyy" value="{{fechamov::input}}" require>
          </div>
        </div>

        <div class="mb-3">
          <label for="concepto">Concepto</label>
          <input type="text" class="form-control" id="concepto" placeholder="" value="{{concepto::input}}" required>
        </div>

        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="tipomov">Tipo Movimiento</label>
            <select class="custom-select d-block w-100" id="tipomov" value="{{tipomov::input}}" required>
              <option value="">Elegir Tipo...</option>
              <option value="+">Ingreso</option>
              <option value="-">Reintegro</option>
            </select>
          </div>
          <div class="col-md-3 mb-3">
            <label for="importe">Importe</label>
            <input type="text" class="form-control" id="importe" placeholder="999999.99" value="{{importe::input}}" required>
          </div>
        </div>

        <button class="btn btn-warning" on-click="volvercuenta">Volver</button>
        <button class="btn btn-warning" on-click="altamovimiento">Alta</button>
        <span hidden$="[[!error]]">[[msgerror]]</span>

       <iron-ajax
          id="doAltamovimiento"
          url="http://localhost:3000/apitechu/movimientos"
          handle-as="json"
          content-type="application/json"
          method="POST"
          on-response="manageAJAXresponsemov"
          on-error ="showError"
        >
        </iron-ajax>
        <iron-ajax
           id="doActsaldo"
           url="http://localhost:3000/apitechu/actsaldo"
           handle-as="json"
           content-type="application/json"
           method="POST"
           on-response="manageAJAXresponsesaldo"
           on-error ="showError"
         >
         </iron-ajax>
    `;
  }

  static get properties() {
    return {
    };
  }  //  End properties

  volvercuenta(){
      console.log("Botón volvercuenta desde alta movimientos");
      this.error = false;
      this.msgerror = "";
      this.fechamov ="";
      this.concepto ="";
      this.tipomov ="";
      this.importe="";

      this.dispatchEvent(
        new CustomEvent(
          "evento-volvercuenta",
          {
            "detail" : {
              "refresco" : this.refresco
            }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
          }
        )  //end parámetros new CustomEvent
      )  //end parámetros dispatchEvent
  }

altamovimiento(){
  console.log("Botón alta MOVIMIENTO");
  var movData = {
    "idcuenta" : this.idcuentalta,
    "fechamov" : this.fechamov,
    "concepto" : this.concepto,
    "tipomov" : this.tipomov,
    "importe" : this.importe
  }
  console.log("Movimiento es: ");
  console.log(movData);
  this.$.doAltamovimiento.body = JSON.stringify(movData);
  this.$.doAltamovimiento.generateRequest();
}

manageAJAXresponsemov(data){
  //  validación
  if (data.detail.response.status =="NOK"){
    this.error = true;
    this.msgerror = data.detail.response.msg;
  } else {
    var movSaldo = {
        "iban" : this.idcuentalta,
        "tipomov": this.tipomov,
        "importe" : this.importe
      }
      console.log("movSaldo es: ");
      console.log(movSaldo);
      this.$.doActsaldo.body = JSON.stringify(movSaldo);
      this.$.doActsaldo.generateRequest();
      this.msgerror = "";
      this.fechamov ="";
      this.concepto ="";
      this.tipomov ="";
      this.importe="";
  }
}

manageAJAXresponsesaldo(data){
//  console.log("Llegaron los resultados ");
//  console.log(data.detail.response);
//  console.log("Botón pulsado ===========================");
  this.dispatchEvent(
    new CustomEvent(
      "evento-volvercuenta",
      {
        "detail" : {
            "refresco" : true
        }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
      }
    )  //end parámetros new CustomEvent
  )  //end parámetros dispatchEvent
}   //End manageAJAXresponse(

showError(error){
  console.log("Hubo un error ");
  console.log(error);
}  // End showError

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('alta-movimiento', AltaMovimiento);
