import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        .lh-condensed { line-height: 1.25; }

        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <div class="col-md-8 order-md-1">
      <h4 class="mb-3">Datos usuario</h4>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" placeholder="" value="{{nombre::input}}" required>
          </div>

          <div class="col-md-6 mb-3">
            <label for="apellidos">Apellidos</label>
            <input type="text" class="form-control" id="apellidos" placeholder="" value="{{apellidos::input}}" required>
          </div>
        </div>

        <div class="mb-3">
          <label for="email">Email </label>
          <input type="email" class="form-control" id="email" placeholder="you@example.com" value="{{email::input}}" required>
        </div>

        <div class="mb-3">
          <label for="direccion">Dirección</label>
          <input type="text" class="form-control" id="direccion" placeholder="C/ Mayor 25" value="{{direccion::input}}" required>
        </div>

        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="provincia">Provincia</label>
            <select class="custom-select d-block w-100" id="provincia" value="{{hostValue::change}}" required>

            <template id="listaprovincias" is="dom-repeat" items="{{provincias}}">
                <option>{{item.nm}}</option>
            </template>

            </select>
          </div>
          <div class="col-md-3 mb-3">
            <label for="codpostal">Código Postal</label>
            <input type="text" class="form-control" id="codpostal" placeholder="" value="{{codpostal::input}}" required>
          </div>
        </div>

        <div class="mb-3">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" placeholder="" value="{{password::input}}" required>
        </div>
        <button class="btn btn-warning" on-click="volverlogin">Volver</button>
        <button class="btn btn-warning" on-click="altausuario">Alta</button>
        <span hidden$="[[!error]]">[[msgerror]]</span>

        <iron-ajax
          id="getProvincias"
          auto
          url="http://localhost:3000/apitechu/provincias"
          handle-as="json"
          on-response="showProvincias"
        >
        </iron-ajax>

        <iron-ajax
          id="doAltausu"
          url="http://localhost:3000/apitechu/user"
          handle-as="json"
          content-type="application/json"
          method="POST"
          on-response="manageAJAXresponseusu"
          on-error ="showError"
        >
        </iron-ajax>
    `;
  }

  static get properties() {
    return {
      provincias: {
        type : Array
      }, provinciaseleccionada: {
        type : String
      }, hostValue: {
        type: String,
        observer: '_observercombo'
      }
    };
  }  //  End properties

_observercombo(newValue, oldValue) {
//  console.log("Old value provincias was " + oldValue);
//  console.log("New value provincias is " + newValue);
  this.provincia = newValue;
}

  // función showData manejadora de la respuesta
  showProvincias(data){
    console.log("showProvincias Alta usuario");
//    console.log(data.detail.response);
    this.provincias=data.detail.response.provincias;
  }

  volverlogin(){
      console.log("Botón volverlogin en alta de usuario");
      this.dispatchEvent(
        new CustomEvent(
          "evento-volverloginusu",
          {
            "detail" : {
            }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
          }
        )  //end parámetros new CustomEvent
      )  //end parámetros dispatchEvent
  }

  altausuario(){
    console.log("Botón alta usuario en alta-usuario");
    var usuData = {
      "nombre": this.nombre,
      "apellidos": this.apellidos,
      "email": this.email,
      "direccion": this.direccion,
      "provincia": this.provincia,
      "codpostal": this.codpostal,
      "password": this.password
    }
    //  this.$ es para acceder a los elementos del html
    this.$.doAltausu.body = JSON.stringify(usuData);
    // comunica con el back-end lanzando la petición
    this.$.doAltausu.generateRequest();
    console.log("usuData es: ");
    console.log(usuData);
  }

  manageAJAXresponseusu(data){
    console.log("Llegaron los resultados de doAltausu");
    console.log(data.detail.response);
    if (data.detail.response.status =="NOK"){
      this.error = true;
      this.msgerror = data.detail.response.msg;
    } else{
//     si no hay error limpio y lanzo evento para volver al login
      this.error = false;
      this.nombre='';
      this.apellidos='';
      this.email='';
      this.direccion='';
      this.provincia='';
      this.codpostal='';
      this.password='';
      this.dispatchEvent(
      new CustomEvent(
        "evento-volverloginusu",
        {
          "detail" : {
            "error" : this.error
    //        "idusuario" : data.detail.response.idUsuario
          }
        }
      )  //end parámetros new CustomEvent
    )  //end parámetros dispatchEvent
  }
  }   //End manageAJAXresponse(

showError(error){
  console.log("Hubo un error ");
  console.log(error);
}  // End showError

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('alta-usuario', AltaUsuario);
