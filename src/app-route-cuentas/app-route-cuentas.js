import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js'
import '../visor-cuenta-lista/visor-cuenta-lista.js'
import '../visor-mov-lista/visor-mov-lista.js'
import '../alta-movimiento/alta-movimiento.js'
import '../alta-movimiento-div/alta-movimiento-div.js'

/**
 * @customElement
 * @polymer
 */
class AppRouteCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .jumbotron{
          color: #B65E0A;
          background-color:#D68910;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h6 style="margin-left: 30px">Usuario: [[nombre]] [[apellidos]]</h6>
      <h6 style="margin-left: 30px">email: [[email]]</h6>
      <div></div>

      <iron-pages selected="[[componentName2]]" attr-for-selected="component-name2">
        <div component-name2="cuenta"><visor-cuenta-lista id=visorcuentas idusuario="[[idusuario]]" refresco="{{refresco}}"
            on-evento-logout="processEventLogout"
            on-evento-altamovimientos="processEventAltaMovimiento"
            on-evento-consultamovimientos="processEventConsultaMovimientos"
            on-evento-altamovimientosdiv="processEventAltaMovDiv">
        </visor-cuenta-lista></div>
        <div component-name2="consultamovimiento"><visor-mov-lista idcuenta="[[idcuenta]]" refrescomov="{{refrescomov}}"
            on-evento-volver-list-cta="processEventVolverListCta">
        </visor-mov-lista></div>
        <div component-name2="altamovimiento"><alta-movimiento idcuentalta="[[idcuentalta]]"
            on-evento-volvercuenta="processEventVolverCuenta">
        </alta-movimiento></div>
        <div component-name2="altamovimientodiv"><alta-movimiento-div idcuentalta="[[idcuentalta]]"
            on-evento-volvercuenta="processEventVolverCuenta">
        </alta-movimiento-div></div>
      </iron-pages>
      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/users/{{idusuario}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }

  static get properties() {
    console.log("get properties app-route-cuentas");
    return {
      componentName2:{
        type:  String,
        value:"cuenta"
      },
      nombre: {
        type: String
      }, apellidos: {
        type: String
      }, email: {
        type: String
      }, idusuario: {
        type: Number,
        observer: "_idusuarioChanged"
      }, refresco: {
        type: Boolean
      }, refrescomov: {
        type: Boolean
      }
    }
  }  //  End properties

  _idusuarioChanged(newValue, oldValue){
    // comunica con el back-end lanzando la petición
    console.log("idusuario app-route-cuentas " + this.idusuario);
    if (this.idusuario != "" && this.idusuario!= null){
      this.$.getUser.generateRequest();
      console.log("idUsuario value has changed");
      console.log("Old value was " + oldValue);
      console.log("New value is " + newValue);
    }
  }

  showData(data){
    console.log("*********showData");
    console.log(data.detail.response.nombre);
    this.nombre=data.detail.response.nombre;
    this.apellidos=data.detail.response.apellidos;
    this.email=data.detail.response.email;
  }

  processEventVolverCuenta(e){
      console.log("Capturado volvercuenta en app-route-cuentas");
      console.log(e);
      console.log("Después petición volvercuenta en app-route-cuentas");
      this.refresco=e.detail.refresco;
      console.log("Refresco en volvercuenta en app-route-cuentas " + this.refresco);
      this.componentName2="cuenta";
      console.log("Después cambio componente volvercuenta en app-route-cuentas");
  }

  processEventVolverListCta(e){
    console.log("Capturado volverlistcta en app-route-cuentas");
    console.log(e);
    console.log("Refrescomov en volverlistacta en app-route-cuentas " + this.refrescomov);
    this.refresco=e.detail.refresco;
    this.refrescomov=e.detail.refrescomov;
    this.idcuentalta = e.detail.cuentaseleccionada;
    this.componentName2="cuenta";
    this.refresco=false;
    this.refrescomov=false;
  }

  processEventAltaMovimiento(e){
    console.log("Capturado evento altamovimiento divisa en app-route-cuentas");
    console.log(e);
    this.idcuentalta = e.detail.cuentaseleccionada;
    this.componentName2="altamovimiento";
    this.refresco=false;
    this.refrescomov=false;
  }

  processEventAltaMovDiv(e){
    console.log("Capturado evento altamovimiento divisa en app-route-cuentas");
    console.log(e);
    this.idcuentalta = e.detail.cuentaseleccionada;
    this.componentName2="altamovimientodiv";
    this.refresco=false;
    this.refrescomov=false;
  }

  processEventConsultaMovimientos(e){
    console.log("Capturado evento consulta movimiento del emisor");
    console.log(e);
    this.idcuenta = e.detail.cuentaseleccionada;
    this.componentName2="consultamovimiento";
    this.refrescomov=e.detail.refrescomov;
    console.log("consulta movimientos cuenta " + this.idcuenta);
    console.log("Llamada a consulta de movimientos con refresco " + this.refrescomov);
  }

processEventLogout(e){
  console.log("Capturado evento logout en app-route-cuentas");
  this.dispatchEvent(
    new CustomEvent(
      "evento-logout",
      {
        "detail" : {
        }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
      }
    )  //end parámetros new CustomEvent
  )  //end parámetros dispatchEvent
}

}  //  End class
// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('app-route-cuentas', AppRouteCuentas);
