import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class AltaMovimientoDiv extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        .lh-condensed { line-height: 1.25; }

        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <div class="col-md-8 order-md-1">
      <h4 class="mb-3">Datos movimiento cuenta:</h4>
      <h5 class="mb-3">{{idcuentalta::input}}</h5>

        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="fechamov">Fecha</label>
            <input type="date" class="form-control" id="fechamov" placeholder="dd/mm/yyyy" value="{{fechamov::input}}" require>
          </div>
        </div>

        <div class="mb-3">
          <label for="concepto">Concepto</label>
          <input type="text" class="form-control" id="concepto" placeholder="" value="{{concepto::input}}" required>
        </div>

        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="tipomov">Tipo Movimiento</label>
            <select class="custom-select d-block w-100" id="tipomov" value="{{tipomov::input}}" required>
              <option value="">Elegir Tipo...</option>
              <option value="+">Ingreso</option>
              <option value="-">Reintegro</option>
            </select>
          </div>
          <div class="col-md-3 mb-3">
            <label for="importediv">Importe</label>
            <input type="text" class="form-control" id="importediv" placeholder="999999.99" value="{{importediv::input}}" required>
          </div>
          <div class="col-md-4 mb-3">
            <label for="divisa">Divisa</label>
            <select class="custom-select d-block w-100" id="divisa" value="{{hostValue::change}}" required>

            <template id="listadivisas" is="dom-repeat" items="{{divisas}}">
                <option>{{item.AlphabeticCode}}</option>
            </template>

            </select>
          </div>
        </div>

        <div></div>
        <div class="row">
          <div class="col-md-5 mb-3">
            <button class="btn btn-info" on-click="cambioeu">Cambio Euros</button>
          </div>
          <div class="col-md-3 mb-3">
            <label for="cambio">Cambio</label>
            <b><span hidden$="[[!cmbvisible]]">[[cambio]]</span></b>
          </div>
          <div class="col-md-4 mb-3">
            <label for="importe">Importe EUR</label>
            <b><span hidden$="[[!cmbvisible]]">[[importe]]</span></b>
          </div>
        </div>

        </div>

        <button class="btn btn-warning" on-click="volvercuenta">Volver</button>
        <button class="btn btn-warning" on-click="altamovimiento">Alta</button>
        <span hidden$="[[!error]]">[[msgerror]]</span>

        <iron-ajax
          id="doCmbDivEur"
          url="http://localhost:3000/apitechu/cmbdiv/{{divisa}}"
          handle-as="json"
          content-type="application/json"
          on-response="showCambio"
        >
        </iron-ajax>
        <iron-ajax
          id="Divisas"
          auto
          url="http://localhost:3000/apitechu/divisas"
          handle-as="json"
          on-response="showDivisas"
        >
        </iron-ajax>
       <iron-ajax
          id="doAltamovimiento"
          url="http://localhost:3000/apitechu/movimientos"
          handle-as="json"
          content-type="application/json"
          method="POST"
          on-response="manageAJAXresponsemov"
          on-error ="showError"
        >
        </iron-ajax>
        <iron-ajax
           id="doActsaldo"
           url="http://localhost:3000/apitechu/actsaldo"
           handle-as="json"
           content-type="application/json"
           method="POST"
           on-response="manageAJAXresponsesaldo"
           on-error ="showError"
         >
         </iron-ajax>
    `;
  }

//  en iro-ajax si el startus es 200 o 300 lanza el on-response, si no lanza el showError

  static get properties() {
    return {
      divisas: {
        type : Array
      }, divisaseleccionada: {
        type : String
      }, hostValue: {
        type: String,
        observer: '_observercombo'
      }
    };
  }  //  End properties

  _observercombo(newValue, oldValue) {
    console.log("Old value was " + oldValue);
    console.log("New value is " + newValue);
    this.divisa = newValue;
  }

  showDivisas(data){
    console.log("showDivisas Alta movimiento divisa");
//    console.log(data.detail.response);
    this.divisas=data.detail.response.divisas;
//    console.log("divisas ========= " + this.divisas);
  }

  volvercuenta(){
      console.log("Botón volvercuenta desde alta movimientos");
      this.error = false;
      this.msgerror = "";
      this.fechamov ="";
      this.concepto ="";
      this.tipomov ="";
      this.importe="";
      this.importediv="";
      this.cambio="";

      this.dispatchEvent(
        new CustomEvent(
          "evento-volvercuenta",
          {
            "detail" : {
              "refresco" : this.refresco
            }
          }
        )  //end parámetros new CustomEvent
      )  //end parámetros dispatchEvent
  }

cambioeu(){
  console.log("Botón cambio");
  this.cmbvisible=false;
  this.cambio = "";
  this.importe = "";

  console.log("importe divisa " + this.importediv);

  if (this.importediv == null || this.importediv =='') {
    this.error = true;
    this.msgerror= "El importe en divisa es obligatorio";
  } else {
    let partes = /^(\d{1,6})[.](\d{2})$/.exec(this.importediv);
    if (!partes) {
      this.error = true;
      this.msgerror= "El importe en divisa es inválido. Debe tener dos decimales";
    } else{
      this.$.divisaobj = "EUR";
      this.$.doCmbDivEur.generateRequest();
    }
  }
}

showCambio(data){
  console.log("showCambio Alta movimiento divisa");
  console.log(data.detail.response);
  this.cambio = data.detail.response.cambio;
  let importeredondeado = this.importediv * this.cambio;
  importeredondeado = parseFloat(importeredondeado).toFixed(2);
  this.importe = importeredondeado;
  this.cmbvisible=true;
}

altamovimiento(){
  console.log("Botón alta MOVIMIENTO");
  var movData = {
    "idcuenta" : this.idcuentalta,
    "fechamov" : this.fechamov,
    "concepto" : this.concepto,
    "tipomov" : this.tipomov,
    "importe" : this.importe
  }
  console.log("movData es: ");
  console.log(movData);
  this.$.doAltamovimiento.body = JSON.stringify(movData);
  this.$.doAltamovimiento.generateRequest();
}

manageAJAXresponsemov(data){
  //  validación
  if (data.detail.response.status =="NOK"){
    this.error = true;
    this.msgerror = data.detail.response.msg;
  } else {
    var movSaldo = {
        "iban" : this.idcuentalta,
        "tipomov": this.tipomov,
        "importe" : this.importe
      }
      console.log("movSaldo es: ");
      console.log(movSaldo);
      this.$.doActsaldo.body = JSON.stringify(movSaldo);
      this.$.doActsaldo.generateRequest();
      this.msgerror = "";
      this.fechamov ="";
      this.concepto ="";
      this.tipomov ="";
      this.importe="";
      this.importediv="";
      this.cambio="";
  }
}

manageAJAXresponsesaldo(data){
  this.dispatchEvent(
    new CustomEvent(
      "evento-volvercuenta",
      {
        "detail" : {
            "refresco" : true
        }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
      }
    )  //end parámetros new CustomEvent
  )  //end parámetros dispatchEvent
}   //End manageAJAXresponse(

showError(error){
  console.log("Hubo un error ");
  console.log(error);
}  // End showError

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('alta-movimiento-div', AltaMovimientoDiv);
