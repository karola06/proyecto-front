import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  			<div class="checkbox mb-3"></div>
  			<img class="mb-4" src="../../Imagen2.png" alt="" width="200" height="80">
  			<h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <input type="email" placeholder="email" value="{{email::input}}" required/>
  			<div class="checkbox mb-3"></div>
        <input type="password" placeholder="password" value="{{password::input}}" required/>
  			<div class="checkbox mb-3"></div>
        <button class="btn btn-warning" on-click="login">Login</button>
        <button class="btn btn-warning" on-click="altausuario" >Nuevo usuario</button>
        <div></div>
        <span hidden$="[[!error]]">[[msgerror]]</span>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error ="showError"
      >
      </iron-ajax>
    `;
  }

//  en iro-ajax si el startus es 200 o 300 lanza el on-response, si no lanza el showError

  static get properties() {
    return {
      password: {
        type: String
      }, email: {
        type: String
      }, isLogged: {
        type: Boolean,
        value: false
      }
    };
  }  //  End properties

  login(){
    console.log("El usuario ha pulsado el botón login");
    var loginData = {
      "email" : this.email,
      "password" : this.password
    }
    //  this.$ es para acceder a los elementos del html
    this.$.doLogin.body = JSON.stringify(loginData);
    // comunica con el back-end lanzando la petición
    this.$.doLogin.generateRequest();
//    console.log("login data es ");
//    console.log(loginData);
  }   //End login

  altausuario(){
      console.log("Botón alta usuario");
      this.dispatchEvent(
        new CustomEvent(
          "evento-altausuario",
          {
            "detail" : {
            }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
          }
        )  //end parámetros new CustomEvent
      )  //end parámetros dispatchEvent
  }

  manageAJAXresponse(data){
    console.log("Llegaron los resultados del login");
    console.log(data.detail.response);
    if (data.detail.response.status =="NOK"){
      this.error = true;
      this.msgerror = data.detail.response.msg;
    }
    else{
      this.error = false;
      this.msgerror = data.detail.response.msg;
  //  si el login es correcto lanzo el evento
    console.log("Botón pulsado ===========================");
    this.email="";
    this.password="";
    this.dispatchEvent(
      new CustomEvent(
        "evento-login",
        {
          "detail" : {
            "idusuario" : data.detail.response.idUsuario
          }   // end detail se envía lo que se quiera (se podría ver en la consola del navegador)
        }
      )  //end parámetros new CustomEvent
    )  //end parámetros dispatchEvent
    }
  }   //End manageAJAXresponse(

  showError(error){
    console.log("Hubo un error ");
    console.log(error);
  }  // End showError

}  //  End class

// define una etiqueta y la asocia con la clase (LoginUsuario)
window.customElements.define('login-usuario', LoginUsuario);
